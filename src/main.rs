use std::fmt;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(raw(setting = "structopt::clap::AppSettings::ColoredHelp"))]
#[structopt(rename_all = "kebab-case")]
/// slit is like cut, but smarter
struct Cli {
    #[structopt(short, long)]
    /// fields to include
    fields: Option<String>,

    #[structopt(short = "F", long)]
    /// fields to exclude
    exclude_fields: Option<String>,

    #[structopt(long, default_value = "0")]
    /// number of lines to skip from beginning of stream
    skip: usize,

    #[structopt(short, long)]
    /// field delimiter to separate fields
    delimiter: Option<String>,

    #[structopt(short = "D", long, default_value = "\t")]
    /// field delimiter to separate fields on output
    output_delimiter: String,

    #[structopt(short, long)]
    /// enable verbose error output
    verbose: bool,
}

enum Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "error")
    }
}

fn run(args: Cli) -> Result<(), Error> {
    if args.verbose {
        eprintln!("verbosity enabled");
    }

    Ok(())
}

fn main() {
    std::process::exit(match run(Cli::from_args()) {
        Ok(_) => 0,
        Err(err) => {
            eprintln!("{}", err);
            1
        }
    })
}
